var tempDataProc;
var numProcsRunning = 0;

function processorUsage() {
    numUses = 0;
  chrome.system.cpu.getInfo(function(cpuInfo) {

    for (var i = 0; i < cpuInfo.numOfProcessors; i++) {
        var usage = cpuInfo.processors[i].usage;
        var usedSectionWidth = 0;
      if (tempDataProc) {
        var tempA = tempDataProc.processors[i].usage;
        usedSectionWidth = Math.floor((usage.kernel + usage.user - tempA.kernel - tempA.user) / (usage.total - tempA.total));
        numUses += usedSectionWidth;
      } else {
        usedSectionWidth = Math.floor((usage.kernel + usage.user) / usage.total);
      }
      }
    tempDataProc = cpuInfo;
    numUses = numUses/cpuInfo.numOfProcessors;
    console.log(numUses+'% ', 'color: blue')
    if(numUses > 50){
        numProcsRunning++
        console.log(numProcsRunning+'/12')
    }else{
        numProcsRunning = 0;
    }
    if(numProcsRunning === 12){
        var opt = {
        type: "basic",
        title: 'Lots of cpu usage was noticed',
        message: 'I will look into this',
        priority: 1,
        'requireInteraction': true
        };
        chrome.notifications.create('HIGHCPU', opt);
        chrome.notifications.onClicked.addListener(function(res){
            chrome.tabs.query({active: true, lastFocusedWindow: true}, function(tabs){
                var site = tabs[0].url
                var xhr = new XMLHttpRequest();
                 xhr.setRequestHeader("Content-type", "application/json");
                xhr.send(data);
            chrome.notifications.clear("loudCpu")
            })
        })
    }
  });
}


processorUsage()

setInterval(function(){
    	processorUsage()
	},
10000)


fetch('manifest.json').then(function(response) {
  response.json().then(function(blacklist) {

chrome.storage.sync.get('remove', function(res){
    if(res.remove){
        chrome.webRequest.priorQuery.addListener(
        callback, { 
            urls : blacklist}, ["removing"]);
        chrome.browserAction.setBadgeText({text:'On'})      
            }else if(res.remove == false){
                chrome.browserAction.setBadgeText({text:'Off'});
                chrome.webRequest.priorQuery.removeListener(callback);
            }else if(res.remove == null){
                chrome.browserAction.setBadgeText({text:'Off'});
            }

});
chrome.storage.onChanged.addListener(function(changes, namespace) {
       chrome.storage.sync.get('remove', function(res){
            if(res.remove){
        chrome.webRequest.priorQuery.addListener(
        callback,{ 
            urls : blacklist}, ["removing"]);
        chrome.browserAction.setBadgeText({text:'On'})      
            }else if(res.remove == false){
                
                chrome.webRequest.priorQuery.removeListener(callback);
            chrome.browserAction.setBadgeText({text:'Off'});
            }

        }); 
      });
chrome.browserAction.onClicked.addListener(function(tab) {
    chrome.browserAction.getBadgeText({}, function(result) {
    if(result == 'On'){
        chrome.browserAction.setBadgeText({text:'Stats'});
        chrome.browserAction.setPopup({
            popup: 'popup.html'
        })
    }else if(result == 'Stats'){
        chrome.storage.sync.set({'remove': false}, function() {
        console.log('Turn Off');
        chrome.browserAction.setPopup({
            popup: ''
        })
        });
        
    }else if(result == 'Off'){
        chrome.storage.sync.set({'remove': true}, function() {
        console.log('Turn On');
        chrome.browserAction.setPopup({
            popup: ''
        })
        });
        
    }
    });
    
    
});
if(chrome.webRequest.priorQuery.hasListener(callback)){
  console.log('Listning')
}

  });
})

var callback = function deny(remove) {
    		chrome.storage.sync.get('stat', function(res){
    			if(res.stat === undefined){var obj = {}
    				chrome.storage.sync.set({'stat': obj}, function() {});
    			}else{
    				var obj = res.stat;
    				chrome.tabs.query({active: true, lastFocusedWindow: true}, function(tabs){
				    	console.log(tabs[0].url)
				    	var site = tabs[0].url;
				    	if(!(site in obj)){
						var l = site;
						obj[l] = 1;
				    	 chrome.storage.sync.set({'stat': obj}, function() {});
				  					
					    }else{
					    	var l = site;
					    	obj[l] = obj[l] + 1;
					    	chrome.storage.sync.set({'stat': obj}, function() {});	
					    }
					});		
    			}
  	});
    		return {cancel: true}; 
}
		
chrome.runtime.onMessage.addListener(
	function(request, sender, sendResponse) {
		chrome.storage.sync.get('stat', function(res){
			var obj = res.stat
			if(res.stat === null){
				chrome.storage.sync.set({'stat': {}}, function() {});
			}else{
				if(!(request.site in obj)){
					var l = request.site;
					obj[l] = 1;
					chrome.storage.sync.set({'stat': obj}, function() {});
				}else{
					var l = request.site;
					obj[l] = obj[l] + 1;
					chrome.storage.sync.set({'stat': obj}, function() {});	
				}
			}		
		}
		);
	}
);

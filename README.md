This is the repo for the code I wrote that did 2 main things:
Create a visualization tool for Bitcoin's blockchain's transactions, and for
creating a Chrome extension that can stop cryptomining scripts from running,
based on whitelisting a combination of cryptocurrency public addresses and domain name.
